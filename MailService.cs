﻿using System;

namespace EventsAndDelegates
{
    public class MailService
    {
        // This class is responsible for sending an email once the video is encoded
        // this is the handler that will be called once the event is fired
        public void OnVideoEncoded(object source, VideoEventArgs args)
        {
            Console.WriteLine("MailService: Sending an email..." + args.Video.Title);
        }
    }
}
