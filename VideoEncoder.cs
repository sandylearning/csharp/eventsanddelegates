﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventsAndDelegates
{
    public class VideoEventArgs : EventArgs
    {
        public Video Video { get; set; }
    }

    public class VideoEncoder
    {
        // Create an event in the VideoEncoder such that it notifies anyone interested once the encoding is complete
        // 1 - Define a delegate - it determines the signature of the event handler method in Subscriber
        // 2 - Define an event based on the delegate
        // 3 - Raise the event

        // 1 - define the delegate - there is a .NET convention to name it with _EventHandler and for their to be 2 parms
        //          object as source and args for anything else you need along with it
        //          here we are creating our own version of EventArgs so we can pass a video
        //public delegate void VideoEncodedEventHandler(object source, VideoEventArgs args);
        // 2- Define an event based on the delegate
        //  use past tense in this case
        //public event VideoEncodedEventHandler VideoEncoded;

        // We can instead use this shorthand format which will replace the above 2 lines
        // then we don't have to create a delegate explicitly
        public event EventHandler<VideoEventArgs> VideoEncoded;

        // Note: if you just need an event with the default signature of (object source, EventArgs args), you can use
        //     public event EventHandler VideoEncoded;

        public void Encode(Video video)
        {
            Console.WriteLine("Encoding Video");
            Thread.Sleep(3000);

            // when the encoding is complete, notify the subscribers
            OnVideoEncoded(video);
        }

        // 3 - Raise the event
        //  .NET convention that the methods should be protected, virtual, and void
        //  naming should be "On" followed by the name of the event
        protected virtual void OnVideoEncoded(Video video)
        {
            // check to see if there are any subscribers
            if (VideoEncoded != null)
            {
                // raise the event, the source is this and you can use EventArgs.Empty if you don't need to pass anything
                // this will notify all of the subscribers
                VideoEncoded(this, new VideoEventArgs() { Video = video });
            }
                
        }
    }

    // Delegate: is an agreement or contract between publisher and subscriber
        // it determines the signature of the event handler method in Subscriber
}
