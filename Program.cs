﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsAndDelegates
{
    // Events are a mechanism for communication between objects
    //  They are helpful in building loosely coupled applications
    //  and helps creating extenssible applications
    class EventsDemonstration
    {
        static void Main(string[] args)
        {
            var video = new Video() { Title = "Video 1" };
            var videoEncoder = new VideoEncoder(); // publisher
            var mailService = new MailService(); // subscriber
            var messageService = new MessageService(); // subscriber

            // before we call the encode method, we need to do the subscription
            // this notation adds a new subscriber to the event - register a handler for the event
            videoEncoder.VideoEncoded += mailService.OnVideoEncoded;
            videoEncoder.VideoEncoded += messageService.OnVideoEncoded;
  
            videoEncoder.Encode(video);
        }
    }
}
